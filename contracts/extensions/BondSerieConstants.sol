// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

abstract contract BondSerieConstants {
    bytes4 public constant BOND_SERIE_INITIALIZER_SELECTOR =
        bytes4(keccak256('__BondSerie_init(address,uint8)'));
}
