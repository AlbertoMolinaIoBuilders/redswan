// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

abstract contract EquitySerieConstants {
    bytes4 public constant EQUITY_SERIE_INITIALIZER_SELECTOR =
        bytes4(keccak256('__EquitySerie_init(address,uint8)'));
}
