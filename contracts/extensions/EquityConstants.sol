// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {EquitySerieConstants} from './EquitySerieConstants.sol';

abstract contract EquityConstants is EquitySerieConstants {
    bytes32 constant DIVIDEND_CORPORATE_ACTION_TYPE =
        keccak256('DIVIDEND_CORPORATE_ACTION_TYPE');

    bytes32 constant SPLIT_CORPORATE_ACTION_TYPE =
        keccak256('SPLIT_CORPORATE_ACTION_TYPE');
}
