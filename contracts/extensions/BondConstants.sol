// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {BondSerieConstants} from './BondSerieConstants.sol';

abstract contract BondConstants is BondSerieConstants {
    bytes32 constant COUPON_CORPORATE_ACTION_TYPE =
        keccak256('COUPON_CORPORATE_ACTION_TYPE');
}
