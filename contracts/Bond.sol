// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {BondConstants} from './extensions/BondConstants.sol';
import {Security} from './layer_1/Security.sol';
import {IBond} from './interfaces/IBond.sol';
import {ISerie} from './layer_1/interfaces/ISerie.sol';
import {IBondSerie} from './interfaces/IBondSerie.sol';
import {
    IScheduledSnapshots
} from './layer_1/extensions/interfaces/IScheduledSnapshots.sol';
import {
    ERC20SnapshotUpgradeable
} from '@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20SnapshotUpgradeable.sol';

/*
Bond is a Security that defines the following corporate actions:
    - Coupon

Bonds use one single Serie whose metadata has the BondMetadata structure
*/

contract Bond is IBond, Security, BondConstants {
    /**
     * @dev Constructor
     *
     * @param beaconAddress The address of the beacon contract pointing to the contract implementing the Series
     */
    constructor(
        address beaconAddress
    ) Security(beaconAddress, BOND_SERIE_INITIALIZER_SELECTOR) {}

    /////////////////////////////////////////
    // BOND SERIES /////////////////////////////////////////
    /////////////////////////////////////////
    /**
     * @dev Creates a new Bond Serie.
     *
     * @param bondMetadata The metadata that will be assigned to the new bond serie
     * @param metadata The metadata from any upper layer (if any)
     * @param initializationArguments The initialization data required to initialize any upper layer
     */
    function createBondSerie(
        IBondSerie.BondMetadata memory bondMetadata,
        bytes memory metadata,
        uint8 initialDecimals,
        bytes memory initializationArguments
    ) external virtual override returns (bool, uint256) {
        bytes memory initArguments = abi.encodePacked(
            uint256(uint160(address(this))),
            uint256(initialDecimals),
            initializationArguments
        );

        return _setSerie(abi.encode(bondMetadata, metadata), initArguments);
    }

    /**
     * @dev Updates an existing Bond Serie metadata.
     * @dev Series can be updated but corporate actions that have been already registered for the future will STILL apply even if the new series properties define otherwise
     *
     * @param serieId The bond serie Id (index + 1)
     * @param newBondMetadata The metadata that will replace the current metadata of the bond serie
     * @param newMetadata The metadata that will replace the current metadata of any upper layer (if any)
     */
    function updateBondSerie(
        uint256 serieId,
        IBondSerie.BondMetadata memory newBondMetadata,
        bytes memory newMetadata
    )
        external
        virtual
        override
        checkPartitionIdExists(serieId)
        isSerieActive(_getPartitionAddress(serieId - 1))
        returns (bool)
    {
        return
            _updateSerie(
                _getPartitionAddress(serieId - 1),
                abi.encode(newBondMetadata, newMetadata)
            );
    }

    /**
     * @dev Removes an existing Bond Serie.
     *
     * @param serieId The bond serie Id
     */
    function removeBondSerie(
        uint256 serieId
    )
        external
        virtual
        override
        checkPartitionIdExists(serieId)
        isSerieActive(_getPartitionAddress(serieId - 1))
        returns (bool)
    {
        return _removeSerie(_getPartitionAddress(serieId - 1));
    }

    /**
     * @dev returns the bond serie metadata and status.
     *
     * @param serieId The bond serie Id
     */
    function getBondSerie(
        uint256 serieId
    )
        external
        view
        virtual
        override
        checkPartitionIdExists(serieId)
        checkPartitionExists(_getPartitionAddress(serieId - 1))
        returns (
            address,
            ISerie.SerieMetadata memory,
            IBondSerie.BondMetadata memory,
            bytes memory
        )
    {
        return _getBondSerie(serieId - 1);
    }

    function _getBondSerie(
        uint256 serieIndex
    )
        internal
        view
        virtual
        returns (
            address,
            ISerie.SerieMetadata memory,
            IBondSerie.BondMetadata memory,
            bytes memory
        )
    {
        address bondSerieAddress = _getPartitionAddress(serieIndex);
        (
            ISerie.SerieMetadata memory serieMetadata,
            bytes memory data
        ) = _getSerie(bondSerieAddress);

        IBondSerie.BondMetadata memory bondMetadata;
        bytes memory metadata;

        if (data.length > 0)
            (bondMetadata, metadata) = abi.decode(
                data,
                (IBondSerie.BondMetadata, bytes)
            );

        return (bondSerieAddress, serieMetadata, bondMetadata, metadata);
    }

    /**
     * @dev returns the bond serie count.
     *
     */
    function getBondSerieCount()
        external
        view
        override
        returns (uint256 _count)
    {
        _count = _getSeriesCount();
    }

    /////////////////////////////////////////
    // COUPONS /////////////////////////////////////////
    /////////////////////////////////////////
    /**
     * @dev Adds a new Coupon.
     *
     * @param newCoupon The properties of the new coupon
     */
    function setCoupon(
        Coupon calldata newCoupon,
        bytes memory metadata
    )
        external
        override
        checkDates(newCoupon.recordDate, newCoupon.executionDate)
        checkPartitionIdExists(newCoupon.serieId)
        returns (bool, uint256)
    {
        (uint256 actionId, uint256 actionIdForType) = _addCorporateAction(
            COUPON_CORPORATE_ACTION_TYPE,
            abi.encode(newCoupon, metadata)
        );

        address serieAddress = _getPartitionAddress(newCoupon.serieId - 1);

        require(
            _getSerieStatus(serieAddress),
            'The bond serie you want to assign the coupon to is not active'
        );

        IScheduledSnapshots(serieAddress).addScheduledSnapshot(
            newCoupon.recordDate,
            abi.encode(actionId)
        );

        _addCorporateActionSnapshotSerie(actionId, newCoupon.serieId);

        return (true, actionIdForType);
    }

    /**
     * @dev returns the properties and related snapshot (if any and at most just one) of a coupon.
     *
     * @param couponId The coupon Id
     */
    function getCoupon(
        uint256 couponId
    )
        external
        view
        override
        checkIndexForCorporateActionByType(
            COUPON_CORPORATE_ACTION_TYPE,
            couponId
        )
        returns (RegisteredCoupon memory, bytes memory)
    {
        uint256 actionId = _getCorporateActionIdByType(
            COUPON_CORPORATE_ACTION_TYPE,
            couponId
        );

        CorporateAction memory couponCorporateAction = _getCorporateAction(
            actionId
        );

        RegisteredCoupon memory resgisteredCoupon;
        bytes memory data;

        if (couponCorporateAction.properties.length > 0)
            (resgisteredCoupon.coupon, data) = abi.decode(
                couponCorporateAction.properties,
                (Coupon, bytes)
            );

        uint256[][] memory snapshotIds = _getSnapshotsID(actionId);

        if (snapshotIds.length != 0)
            resgisteredCoupon.snapshotId = snapshotIds[0][1];

        return (resgisteredCoupon, data);
    }

    /**
     * @dev returns the coupons count.
     *
     */
    function getCouponsCount() external view override returns (uint256) {
        return _getCorporateActionsCountByType(COUPON_CORPORATE_ACTION_TYPE);
    }
}
