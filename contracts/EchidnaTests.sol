// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {EquitySerie} from './EquitySerie.sol';
import {Equity} from './Equity.sol';
import {BondSerie} from './BondSerie.sol';
import {Bond} from './Bond.sol';

uint8 constant INIT_DECIMALS = 8;
address constant ADDRESS_1 = address(0x01);

contract Test_EquitySerie is EquitySerie {
    uint256 constant sender_init_amount = 1175;
    uint256 constant totalSupply_init = 2250;
    uint256 constant max_factor = 10000000000;
    address creator;
    uint256 expected_division;
    uint256 expected_module;

    constructor() {
        Equity equity = new Equity(ADDRESS_1);
        __EquitySerie_init(address(equity), INIT_DECIMALS);
        creator = msg.sender;
        _mint(creator, sender_init_amount);
        _mint(address(1), totalSupply_init - sender_init_amount);
    }

    function echidna_lastFactor_under_aggregatedFactor()
        public
        view
        returns (bool)
    {
        return lastFactorOf(msg.sender) <= getAggregatedFactor();
    }

    function assert_SplittingNoDecimals_keeps_balances(
        uint256 factor,
        uint8 subtractDecimals
    ) public {
        require((factor * 10 ** subtractDecimals) <= max_factor);
        setExpected();

        uint8 expected_decimals = 0;
        uint256 factor_2 = factor;

        if (subtractDecimals > expected_decimals)
            factor_2 *= 10 ** (subtractDecimals - decimals());
        else expected_decimals = decimals() - subtractDecimals;
        if (factor_2 > 0) expected_module *= factor_2;

        EquitySerie(address(this)).SplittingNoDecimals(factor, subtractDecimals);

        check_balances(expected_decimals);
    }

    function assert_SplittingWithDecimals_keeps_balances(
        uint256 factor,
        uint8 addDecimals
    ) public {
        require(factor <= max_factor);
        setExpected();

        uint8 expected_decimals = decimals() + addDecimals;

        if (factor > 0) expected_module *= factor;

        EquitySerie(address(this)).SplittingWithDecimals(factor, addDecimals);

        check_balances(expected_decimals);
    }

    function check_balances(uint8 expected_decimals) internal view {
        uint256 actual_division = totalSupply() / balanceOf(creator);
        uint256 actual_module = totalSupply() % balanceOf(creator);
        uint8 actual_decimals = decimals();

        assert(expected_division == actual_division);
        assert(expected_module == actual_module);
        assert(expected_decimals == actual_decimals);
    }

    function setExpected() internal {
        expected_division = totalSupply() / balanceOf(creator);
        expected_module = totalSupply() % balanceOf(creator);
    }
}

contract Test_BondSerie is BondSerie {
    constructor() {
        Bond bond = new Bond(ADDRESS_1);
        __BondSerie_init(address(bond), INIT_DECIMALS);
    }

    function echidna_dumb() public pure returns (bool) {
        return true;
    }
}

contract Test_Bond is Bond {
    constructor() Bond(ADDRESS_1) {}

    function echidna_dumb() public pure returns (bool) {
        return true;
    }
}

contract Test_Equity is Equity {
    constructor() Equity(ADDRESS_1) {}

    function echidna_dumb() public pure returns (bool) {
        return true;
    }
}
