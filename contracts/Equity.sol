// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {EquityConstants} from './extensions/EquityConstants.sol';
import {Security} from './layer_1/Security.sol';
import {IEquity} from './interfaces/IEquity.sol';
import {ISerie} from './layer_1/interfaces/ISerie.sol';
import {IEquitySerie} from './interfaces/IEquitySerie.sol';
import {
    IScheduledSnapshots
} from './layer_1/extensions/interfaces/IScheduledSnapshots.sol';
import {
    ERC20SnapshotUpgradeable
} from '@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20SnapshotUpgradeable.sol';

/*
Equity is a Security that defines the following corporate actions:
    - Dividends

Equities use EquitySeries which are Series whose metadata have the EquityMetadata structure
*/

contract Equity is IEquity, Security, EquityConstants {
    /**
     * @dev Constructor
     *
     * @param beaconAddress The address of the beacon contract pointing to the contract implementing the Series
     */
    constructor(
        address beaconAddress
    ) Security(beaconAddress, EQUITY_SERIE_INITIALIZER_SELECTOR) {}

    /////////////////////////////////////////
    // EQUITY SERIES /////////////////////////////////////////
    /////////////////////////////////////////
    /**
     * @dev Creates a new Equity Serie.
     *
     * @param equityMetadata The metadata that will be assigned to the new equity serie
     * @param metadata The metadata from any upper layer (if any)
     * @param initializationArguments The initialization data required to initialize any upper layer
     */
    function createEquitySerie(
        IEquitySerie.EquityMetadata calldata equityMetadata,
        bytes memory metadata,
        uint8 initialDecimals,
        bytes memory initializationArguments
    ) external virtual override returns (bool, uint256) {
        bytes memory initArguments = abi.encodePacked(
            uint256(uint160(address(this))),
            uint256(initialDecimals),
            initializationArguments
        );

        return _setSerie(abi.encode(equityMetadata, metadata), initArguments);
    }

    /**
     * @dev Updates an existing Equity Serie metadata.
     * @dev Series can be updated but corporate actions that have been already registered for the future will STILL apply even if the new series properties define otherwise
     *
     * @param serieId The equity serie Id (index + 1)
     * @param newEquityMetadata The metadata that will replace the current metadata of the equity serie
     * @param newMetadata The metadata that will replace the current metadata of any upper layer (if any)
     */
    function updateEquitySerie(
        uint256 serieId,
        IEquitySerie.EquityMetadata calldata newEquityMetadata,
        bytes memory newMetadata
    )
        external
        virtual
        override
        checkPartitionIdExists(serieId)
        isSerieActive(_getPartitionAddress(serieId - 1))
        returns (bool)
    {
        return
            _updateSerie(
                _getPartitionAddress(serieId - 1),
                abi.encode(newEquityMetadata, newMetadata)
            );
    }

    /**
     * @dev Removes an existing Equity Serie.
     *
     * @param serieId The equity serie Id
     */
    function removeEquitySerie(
        uint256 serieId
    )
        external
        virtual
        override
        checkPartitionIdExists(serieId)
        isSerieActive(_getPartitionAddress(serieId - 1))
        returns (bool)
    {
        return _removeSerie(_getPartitionAddress(serieId - 1));
    }

    /**
     * @dev returns the equity serie metadata and status.
     *
     * @param serieId The equity serie Id
     */
    function getEquitySerie(
        uint256 serieId
    )
        external
        view
        virtual
        override
        checkPartitionIdExists(serieId)
        checkPartitionExists(_getPartitionAddress(serieId - 1))
        returns (
            address,
            ISerie.SerieMetadata memory,
            IEquitySerie.EquityMetadata memory,
            bytes memory
        )
    {
        return _getEquitySerie(serieId - 1);
    }

    function _getEquitySerie(
        uint256 serieIndex
    )
        internal
        view
        virtual
        returns (
            address,
            ISerie.SerieMetadata memory,
            IEquitySerie.EquityMetadata memory,
            bytes memory
        )
    {
        address equitySerieAddress = _getPartitionAddress(serieIndex);
        (
            ISerie.SerieMetadata memory serieMetadata,
            bytes memory data
        ) = _getSerie(equitySerieAddress);

        IEquitySerie.EquityMetadata memory equityMetadata;
        bytes memory metadata;

        if (data.length > 0)
            (equityMetadata, metadata) = abi.decode(
                data,
                (IEquitySerie.EquityMetadata, bytes)
            );

        return (equitySerieAddress, serieMetadata, equityMetadata, metadata);
    }

    /**
     * @dev returns the equity serie count.
     *
     */
    function getEquitySerieCount()
        external
        view
        override
        returns (uint256 _count)
    {
        _count = _getSeriesCount();
    }

    /////////////////////////////////////////
    // DIVIDENDS /////////////////////////////////////////
    /////////////////////////////////////////
    /**
     * @dev Adds a new Dividend.
     *
     * @param newDividend The properties of the new dividend
     * @param metadata the upper layer metadata
     */
    function setDividends(
        Dividend calldata newDividend,
        bytes memory metadata
    )
        external
        virtual
        override
        checkDates(newDividend.recordDate, newDividend.executionDate)
        returns (bool, uint256)
    {
        (uint256 actionId, uint256 actionIdForType) = _addCorporateAction(
            DIVIDEND_CORPORATE_ACTION_TYPE,
            abi.encode(newDividend, metadata)
        );

        uint256 seriesCount = _getSeriesCount();

        for (uint256 serieIndex = 0; serieIndex < seriesCount; serieIndex++) {
            address serieAddress = _getPartitionAddress(serieIndex);

            if (!_getSerieStatus(serieAddress)) continue;

            (
                ,
                ,
                IEquitySerie.EquityMetadata memory equityMetadata,

            ) = _getEquitySerie(serieIndex);

            if (
                (newDividend.preferred && !equityMetadata.prefDividends) ||
                (!newDividend.preferred && !equityMetadata.commonDividends)
            ) continue;

            IScheduledSnapshots(serieAddress).addScheduledSnapshot(
                newDividend.recordDate,
                abi.encode(actionId)
            );

            _addCorporateActionSnapshotSerie(actionId, serieIndex + 1);
        }

        return (true, actionIdForType);
    }

    /**
     * @dev returns the properties and related snapshots (if any) of a dividend.
     *
     * @param dividendId The dividend Id
     */
    function getDividends(
        uint256 dividendId
    )
        external
        view
        override
        checkIndexForCorporateActionByType(
            DIVIDEND_CORPORATE_ACTION_TYPE,
            dividendId
        )
        returns (RegisteredDividend memory, bytes memory)
    {
        uint256 actionId = _getCorporateActionIdByType(
            DIVIDEND_CORPORATE_ACTION_TYPE,
            dividendId
        );

        CorporateAction memory dividendCorporateAction = _getCorporateAction(
            actionId
        );

        RegisteredDividend memory registeredDividend;
        bytes memory data;

        if (dividendCorporateAction.properties.length > 0)
            (registeredDividend.dividend, data) = abi.decode(
                dividendCorporateAction.properties,
                (Dividend, bytes)
            );

        registeredDividend.serieIds = _getSnapshotsSeriesID(actionId);

        registeredDividend.snapshotIds = _getSnapshotsID(actionId);

        return (registeredDividend, data);
    }

    /**
     * @dev returns the dividends count.
     *
     */
    function getDividendsCount() external view override returns (uint256) {
        return _getCorporateActionsCountByType(DIVIDEND_CORPORATE_ACTION_TYPE);
    }

    /////////////////////////////////////////
    // SPLITS /////////////////////////////////////////
    /////////////////////////////////////////
    /**
     * @dev Adds a new Split.
     *
     * @param newSplit The properties of the new split
     * @param metadata the upper layer metadata
     */
    function setSplit(
        Split calldata newSplit,
        bytes memory metadata
    ) external virtual override returns (bool, uint256) {
        (uint256 actionId, uint256 actionIdForType) = _addCorporateAction(
            SPLIT_CORPORATE_ACTION_TYPE,
            abi.encode(newSplit, metadata)
        );

        uint256 seriesCount = _getSeriesCount();

        for (uint256 serieIndex = 0; serieIndex < seriesCount; serieIndex++) {
            address serieAddress = _getPartitionAddress(serieIndex);

            if (!_getSerieStatus(serieAddress)) continue;

            _addCorporateActionSnapshotSerie(actionId, serieIndex + 1);
            uint256 snapshotId = IScheduledSnapshots(serieAddress)
                .takeSnapshot();
            _addCorporateActionSnapshot(actionId, snapshotId, serieAddress);

            if (newSplit.add)
                IEquitySerie(serieAddress).SplittingWithDecimals(
                    newSplit.factor,
                    newSplit.decimals
                );
            else
                IEquitySerie(serieAddress).SplittingNoDecimals(
                    newSplit.factor,
                    newSplit.decimals
                );
        }

        return (true, actionIdForType);
    }

    /**
     * @dev returns the properties of a split.
     *
     * @param splitId The split Id
     */
    function getSplit(
        uint256 splitId
    )
        external
        view
        override
        checkIndexForCorporateActionByType(SPLIT_CORPORATE_ACTION_TYPE, splitId)
        returns (RegisteredSplit memory, bytes memory)
    {
        uint256 actionId = _getCorporateActionIdByType(
            SPLIT_CORPORATE_ACTION_TYPE,
            splitId
        );

        CorporateAction memory splitCorporateAction = _getCorporateAction(
            actionId
        );

        RegisteredSplit memory registeredSplit;
        bytes memory data;

        if (splitCorporateAction.properties.length > 0)
            (registeredSplit.split, data) = abi.decode(
                splitCorporateAction.properties,
                (Split, bytes)
            );

        registeredSplit.serieIds = _getSnapshotsSeriesID(actionId);

        registeredSplit.snapshotIds = _getSnapshotsID(actionId);

        return (registeredSplit, data);
    }

    /**
     * @dev returns the splits count.
     *
     */
    function getSplitsCount() external view override returns (uint256) {
        return _getCorporateActionsCountByType(SPLIT_CORPORATE_ACTION_TYPE);
    }
}
