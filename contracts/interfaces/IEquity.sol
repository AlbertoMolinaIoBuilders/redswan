// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {ISerie} from '../layer_1/interfaces/ISerie.sol';
import {IEquitySerie} from './IEquitySerie.sol';

interface IEquity {
    //////////////////////
    // Equity Series //////////////////////
    //////////////////////

    /* Methods*/
    function createEquitySerie(
        IEquitySerie.EquityMetadata calldata equityMetadata,
        bytes memory metadata,
        uint8 initialDecimals,
        bytes memory initializationArguments
    ) external returns (bool, uint256);

    function updateEquitySerie(
        uint256 serieId,
        IEquitySerie.EquityMetadata calldata equityMetadata,
        bytes memory newMetadata
    ) external returns (bool);

    function removeEquitySerie(uint256 serieId) external returns (bool);

    function getEquitySerie(
        uint256 serieId
    )
        external
        view
        returns (
            address,
            ISerie.SerieMetadata memory,
            IEquitySerie.EquityMetadata memory,
            bytes memory
        );

    function getEquitySerieCount() external view returns (uint256);

    //////////////////////
    // Dividends //////////////////////
    //////////////////////

    /* Data Structures */
    struct Dividend {
        uint256 recordDate;
        uint256 executionDate;
        uint256 amount;
        bool preferred;
    }

    struct RegisteredDividend {
        Dividend dividend;
        uint256[] serieIds;
        uint256[][] snapshotIds;
    }

    /* Methods*/
    function setDividends(
        Dividend calldata newDividend,
        bytes memory metadata
    ) external returns (bool, uint256);

    function getDividends(
        uint256 dividendId
    ) external view returns (RegisteredDividend memory, bytes memory);

    function getDividendsCount() external view returns (uint256);

    //////////////////////
    // Split //////////////////////
    //////////////////////

    /* Data Structures */
    struct Split {
        uint256 factor;
        uint8 decimals;
        bool add; // will increase the token's decimals thus reducing the overall total supply
    }

    struct RegisteredSplit {
        Split split;
        uint256[] serieIds;
        uint256[][] snapshotIds;
    }

    /* Methods*/
    function setSplit(
        Split calldata newSplit,
        bytes memory metadata
    ) external returns (bool, uint256);

    function getSplit(
        uint256 splitId
    ) external view returns (RegisteredSplit memory, bytes memory);

    function getSplitsCount() external view returns (uint256);
}
