// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

interface IEquitySerie {
    /**
     * data structure
     */
    struct EquityMetadata {
        string ISIN;
        bool voting;
        bool info;
        bool liquidation;
        bool prefDividends;
        bool commonDividends;
        bool subscription;
        bool convertion;
        bool redemption;
        uint256 nominalValue;
    }

    /**
     * methods
     */
    function SplittingNoDecimals(
        uint256 factor,
        uint8 subtractDecimals
    ) external;

    function SplittingWithDecimals(uint256 factor, uint8 addDecimals) external;
}
