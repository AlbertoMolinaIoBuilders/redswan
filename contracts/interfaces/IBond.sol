// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {ISerie} from '../layer_1/interfaces/ISerie.sol';
import {IBondSerie} from './IBondSerie.sol';

interface IBond {
    //////////////////////
    // Bond Series //////////////////////
    //////////////////////

    /* Methods*/
    function createBondSerie(
        IBondSerie.BondMetadata calldata bondMetadata,
        bytes memory metadata,
        uint8 initialDecimals,
        bytes memory initializationArguments
    ) external returns (bool, uint256);

    function updateBondSerie(
        uint256 serieId,
        IBondSerie.BondMetadata calldata bondMetadata,
        bytes memory newMetadata
    ) external returns (bool);

    function removeBondSerie(uint256 serieId) external returns (bool);

    function getBondSerie(
        uint256 serieId
    )
        external
        view
        returns (
            address,
            ISerie.SerieMetadata memory,
            IBondSerie.BondMetadata memory,
            bytes memory
        );

    function getBondSerieCount() external view returns (uint256);

    //////////////////////
    // Coupons //////////////////////
    //////////////////////

    /* Data Structures */
    struct Coupon {
        uint256 serieId; // A coupon will apply to only one bond serie
        uint256 recordDate;
        uint256 executionDate;
        uint256 rate;
        bytes32 couponType;
    }

    struct RegisteredCoupon {
        Coupon coupon;
        uint256 snapshotId;
    }

    /* Methods*/
    function setCoupon(
        Coupon calldata newCoupon,
        bytes memory metadata
    ) external returns (bool, uint256);

    function getCoupon(
        uint256 couponId
    ) external view returns (RegisteredCoupon memory, bytes memory);

    function getCouponsCount() external view returns (uint256);
}
