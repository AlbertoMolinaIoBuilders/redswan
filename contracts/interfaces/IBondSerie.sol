// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

interface IBondSerie {
    /* Data Structures */
    struct BondMetadata {
        bool redemption;
        bool liquidation;
        bool convertion;
        bool put;
        uint256 minimalValue;
        uint256 maturityDate;
        uint256 startDate;
    }
}
