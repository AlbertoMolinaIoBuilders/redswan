// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {Serie} from './layer_1/Serie.sol';
import {IEquitySerie} from './interfaces/IEquitySerie.sol';

/*
    Series extend Partitions (as defined at the ERC1400 level) with all the modules that apply :
    - Snapshots : snapshots must be schedulable at the partition level => ScheduledSnapshots
    - Corporate Actions : corporate actions info must be updated when a scheduled snapshot is executed
    - Caps : we must be able to set max supplies per partition

    Series will keep in storage the serie Id (aka partition Id) they have been assigned by their parent Security contract, 
    this information must be provided to the parent Security contract when communicating with it, so that the parent knows
*/

contract EquitySerie is IEquitySerie, Serie {
    // supply changes
    mapping(address => uint256) private _lastFactor;
    uint256 private _aggregatedFactor;

    mapping(address => Snapshots) private _accountlastFactorSnapshots;
    Snapshots private _aggregatedFactorSnapshots;
    Snapshots private _decimalsSnapshots;

    // modifier
    modifier differentThan0(uint256 i) {
        require(i != 0, 'value is 0');
        _;
    }

    /**
     * @dev Constructor required to avoid Initializer attack on logic contract
     */
    constructor() {
        // _disableInitializers();
    }

    // Initialization /////////////////

    function __EquitySerie_init(
        address securityContract,
        uint8 initDecimals
    ) public initializer {
        __Serie_init(securityContract, initDecimals);
        __EquitySerie_init_unchained();
    }

    function __EquitySerie_init_unchained() internal onlyInitializing {
        _aggregatedFactor = 1;
    }

    /**
     * DECIMALS
     */
    function decimalsAt(
        uint256 snapshotId
    ) public view virtual returns (uint256) {
        (bool snapshotted, uint256 value) = _valueAt(
            snapshotId,
            _decimalsSnapshots
        );

        return snapshotted ? value : decimals();
    }

    function _updateDecimalsSnapshot() private {
        _updateSnapshot(_decimalsSnapshots, decimals());
    }

    function _setDecimals(uint8 newDecimals) internal virtual override {
        _updateDecimalsSnapshot();
        super._setDecimals(newDecimals);
    }

    /**
     * AGGREGATED FACTOR
     */
    function aggregatedFactorAt(
        uint256 snapshotId
    ) public view virtual returns (uint256) {
        (bool snapshotted, uint256 value) = _valueAt(
            snapshotId,
            _aggregatedFactorSnapshots
        );

        return snapshotted ? value : getAggregatedFactor();
    }

    function _updateAggregatedFactorSnapshot() private {
        _updateSnapshot(_aggregatedFactorSnapshots, getAggregatedFactor());
    }

    function _setAggregatedFactor(
        uint256 newAggregatedFactor
    ) internal virtual {
        _updateAggregatedFactorSnapshot();
        _aggregatedFactor = newAggregatedFactor;
    }

    function getAggregatedFactor() public view virtual returns (uint256) {
        return _aggregatedFactor;
    }

    /**
     * LAST FACTOR
     */
    function lastFactorOfAt(
        address account,
        uint256 snapshotId
    ) public view virtual returns (uint256) {
        (bool snapshotted, uint256 value) = _valueAt(
            snapshotId,
            _accountlastFactorSnapshots[account]
        );

        return snapshotted ? value : lastFactorOf(account);
    }

    function _updateAccountLastFactorSnapshot(address account) private {
        _updateSnapshot(
            _accountlastFactorSnapshots[account],
            lastFactorOf(account)
        );
    }

    function lastFactorOf(address account) public view returns (uint256) {
        return _lastFactor[account];
    }

    function _setLastFactorOf(
        address account,
        uint256 newLastFactor
    ) internal virtual {
        _updateAccountLastFactorSnapshot(account);
        _lastFactor[account] = newLastFactor;
    }

    /**
     * TOTAL SUPPLY
     */
    function _setTotalSupply(uint256 newTotalSuuply) internal virtual {
        _updateTotalSupplySnapshot();
        _totalSupply = newTotalSuuply;
    }

    /**
     * BALANCE
     */
    function setBalanceOf(
        address account,
        uint256 newBalance
    ) internal virtual {
        _updateAccountSnapshot(account);
        _balances[account] = newBalance;
    }

    // changing Supply

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual override {
        _triggerScheduledSnapshots(0);

        if (from == address(0)) {
            // mint
            updateBalanceOf(to);
            _updateTotalSupplySnapshot();
        } else if (to == address(0)) {
            // burn
            updateBalanceOf(from);
            _updateTotalSupplySnapshot();
        } else {
            // transfer
            updateBalanceOf(from);
            updateBalanceOf(to);
        }
    }

    function updateBalanceOf(address account) internal {
        uint256 lastFactor = lastFactorOf(account);
        uint256 diff_factor = 1;

        if (lastFactor != _aggregatedFactor) {
            if (lastFactor != 0) {
                diff_factor = _aggregatedFactor / lastFactor;
            }
            _setLastFactorOf(account, _aggregatedFactor);
        }

        setBalanceOf(account, balanceOf(account) * diff_factor);
    }

    function balanceOf(
        address account
    ) public view virtual override returns (uint256) {
        uint256 balance = _balances[account];
        if (balance == 0) return balance;
        if (_lastFactor[account] != _aggregatedFactor) {
            uint256 diff_factor = _aggregatedFactor / _lastFactor[account];
            balance *= diff_factor;
        }

        return balance;
    }

    /* 
        Must be used when updating the supply with an integer (no decimals!!!!)
        - Split 3 => factor = 3; decimals = 0;
        - Split 30 => factor = 3; decimals = 1;
        - Split 1000 => factor = 1; decimals = 3;
        - Split 13.55 => NOT POSSIBLE, USE "SplittingWithDecimals for this
    */
    function SplittingNoDecimals(
        uint256 factor,
        uint8 subtractDecimals
    ) external differentThan0(factor) {
        uint8 currentDecimals = decimals();
        if (subtractDecimals > currentDecimals) {
            factor *= 10 ** (subtractDecimals - currentDecimals);
            subtractDecimals = currentDecimals;
        }
        if (subtractDecimals != 0)
            _setDecimals(currentDecimals - subtractDecimals);
        if (factor != 1) {
            _setAggregatedFactor(getAggregatedFactor() * factor);
            _setTotalSupply(totalSupply() * factor);
        }
    }

    /* 
       Must be used when updating the supply with a decimal
        - Split 3.5 => factor = 35; decimals = 1;
        - ReverseSplit 0.3 => factor = 3; decimals = 1;
        - ReverseSplit 0.005 => factor = 5; decimals = 3;

        Caveats : reverse spliting by infinite fractions (like 1/3) will require rounding it up to X decimals (i.e factor = 3333; decimals = 4)
    */
    function SplittingWithDecimals(
        uint256 factor,
        uint8 addDecimals
    ) external differentThan0(factor) {
        if (addDecimals != 0) _setDecimals(decimals() + addDecimals);
        if (factor != 1) {
            _setAggregatedFactor(getAggregatedFactor() * factor);
            _setTotalSupply(totalSupply() * factor);
        }
    }
}
