// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {SerieConstants} from './extensions/SerieConstants.sol';
import {Partition} from './extensions/Partition.sol';
import {ISerie} from './interfaces/ISerie.sol';
import {ISecurity} from './interfaces/ISecurity.sol';

/*
    Series extend Partitions (as defined at the ERC1400 level) with all the modules that apply :
    - Snapshots : snapshots must be schedulable at the partition level => ScheduledSnapshots
    - Corporate Actions : corporate actions info must be updated when a scheduled snapshot is executed
    - Caps : we must be able to set max supplies per partition

    Series will keep in storage the serie Id (aka partition Id) they have been assigned by their parent Security contract, 
    this information must be provided to the parent Security contract when communicating with it, so that the parent knows
*/

contract Serie is ISerie, Partition, SerieConstants {
    // Initializer
    function __Serie_init(
        address securityContract,
        uint8 initDecimals
    ) public onlyInitializing {
        __Partition_init(securityContract, initDecimals);
    }

    function _onScheduledSnapshotTriggered(
        uint256 snapShotID,
        bytes memory data
    ) internal virtual override {
        uint256 referenceID_Integer;
        if (data.length > 0) referenceID_Integer = abi.decode(data, (uint256));

        ISecurity(_SECURITY_CONTRACT).addCorporateActionSnapshot(
            referenceID_Integer,
            snapShotID
        );
    }
}
