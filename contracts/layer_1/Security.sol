// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {SecurityConstants} from './extensions/SecurityConstants.sol';
import {CorporateActions} from './extensions/CorporateActions.sol';
import {ERC1400} from './extensions/ERC1400.sol';
import {ISecurity} from './interfaces/ISecurity.sol';
import {ISerie} from './interfaces/ISerie.sol';
import {IERC20} from '@openzeppelin/contracts/token/ERC20/IERC20.sol';
import {IPartition} from './extensions/interfaces/IPartition.sol';

/*
    Security extends the ERC1400 Standard Security with the extra modules defined in Layer 1 extensions:
    -   Corporate Actions

    A security is divided into non-fungible series.
    Each serie is an independent external contract, more accurately, series are Beacon Proxies that point to the same Beacon.
    Security only keeps a list of all the deployed Series and interacts with them when it needs to operate with tokens.

    Corporate actions are defined in layer 2, which is why a "Security" cannot be deployed as defined in layer 1 unless we do not need to use corporate actions.
*/

contract Security is ISecurity, ERC1400, CorporateActions, SecurityConstants {
    /////////////////////////////////////////
    // MODIFIERS /////////////////////////////////////////
    /////////////////////////////////////////
    /**
     * @dev Checks if execution Date is greater than record Date
     *
     * @param recordDate the record date of a corporate action
     * @param executionDate the execution date of a corporate action
     */
    modifier checkDates(uint256 recordDate, uint256 executionDate) {
        if (executionDate < recordDate)
            revert('Wrong execution and record Dates');
        _;
    }

    /**
     * @dev Checks if a calling contract corresponds to an active serie
     *
     * @param serieAddress address of the calling contract
     */
    modifier isSerieActive(address serieAddress) {
        _checkPartitionExists(serieAddress);
        _isActive(serieAddress);
        _;
    }

    function _isActive(address serieAddress) internal view {
        if (!_getSerieStatus(serieAddress)) revert('serie is not active');
    }

    function _getSerieStatus(
        address serieAddress
    ) internal view returns (bool) {
        (ISerie.SerieMetadata memory serieMetadata, ) = _getSerie(serieAddress);
        return serieMetadata.active;
    }

    /**
     * @dev Constructor
     *
     * @param beaconAddress The address of the beacon contract pointing to the contract implementing the Series
     * @param initializerSelector the beacon proxy initializer method selector
     */
    constructor(
        address beaconAddress,
        bytes4 initializerSelector
    ) ERC1400(beaconAddress, initializerSelector) {}

    //////////////////////////////////////////////////////////////////////
    // Corporate Actions //////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /**
     * @dev Adds a corporate actions snapshot id to the results.
     * This method is meant to be called exclusively from one of the Security's active Series contracts.
     *
     *   Snapshots will be stored in the following way
     *   slot 1 : Serie Id 1 (index + 1)
     *   slot 2 : SnapShot 1
     *   slot 3 : Serie Id 2 (index + 1)
     *   slot 4 : SnapShot 2
     *   ...
     *   slot n-1 : Serie Id n/2 (index + 1)
     *   slot n : SnapShot n/2
     *
     * @param actionId The corporate action Id that must be updated
     * @param snapshotId the snapshot Id to add to the corporate action
     */
    function addCorporateActionSnapshot(
        uint256 actionId,
        uint256 snapshotId
    )
        external
        virtual
        override
        isSerieActive(msg.sender)
        checkIndexForCorporateAction(actionId)
    {
        _addCorporateActionSnapshot(actionId, snapshotId, msg.sender);
    }

    function _addCorporateActionSnapshot(
        uint256 actionId,
        uint256 snapshotId,
        address partitionAddress
    ) internal virtual {
        bytes memory result = abi.encodePacked(
            _getResult(actionId, SNAPSHOT_RESULT_ID),
            _getPartitionId(partitionAddress),
            snapshotId
        );

        _updateCorporateActionResult(actionId, SNAPSHOT_RESULT_ID, result);
    }

    /**
     * @dev Adds a corporate actions serie id to the results.
     * This method is meant to be called when defining the list of series that must trigger a snapshot for an action.
     *
     *   Series will be stored in the following way
     *   slot 1 : Serie Id 1 (index + 1)
     *   slot 2 : Serie Id 2 (index + 1)
     *   ...
     *   slot n : SnapShot n
     *
     * @param actionId The corporate action Id that must be updated
     * @param serieId the serie Id to add to the corporate action
     */

    function _addCorporateActionSnapshotSerie(
        uint256 actionId,
        uint256 serieId
    ) internal virtual {
        bytes memory result = abi.encodePacked(
            _getResult(actionId, SNAPSHOT_LIST_ID),
            serieId
        );

        _updateCorporateActionResult(actionId, SNAPSHOT_LIST_ID, result);
    }

    function _getResult(
        uint256 actionId,
        uint256 resultId
    ) private view returns (bytes memory) {
        bytes memory result;

        if (_getCorporateActionResultsCount(actionId) > resultId)
            result = _getCorporateActionResult(actionId, resultId);

        return result;
    }

    /**
     * @dev returns a two dimensional array of "SerieId - SnpashotId" tuples from a corporate action results list
     * @dev the data must be encodePacked, SerieId and SnapshotId must be uint256.
     * @dev the results list item to be extracted is defined by the constant SNAPSHOT_RESULT_ID
     *
     * @param actionId the corporate action id
     */
    function _getSnapshotsID(
        uint256 actionId
    ) internal view returns (uint256[][] memory) {
        bytes memory data = _getResult(actionId, SNAPSHOT_RESULT_ID);

        uint256 bytesLength = data.length;
        uint256 bytesLengthInWords = bytesLength / 32;
        uint256 totalSnapshots = bytesLengthInWords / 2;

        uint256[][] memory snapshots = new uint256[][](totalSnapshots);

        for (uint256 i = 0; i < bytesLengthInWords; i++) {
            uint256 element;
            uint256 snapshotID = i / 2;
            uint256 itemID = i % 2;
            if (itemID == 0) snapshots[snapshotID] = new uint256[](2);
            assembly {
                element := mload(add(data, add(0x20, mul(i, 0x20))))
            }
            snapshots[snapshotID][itemID] = element;
        }

        return snapshots;
    }

    /**
     * @dev returns an array of "SerieIds" from a corporate action results list
     * @dev the data must be encodePacked, SerieId must be uint256.
     * @dev the results list item to be extracted is defined by the constant SNAPSHOT_LIST_ID
     *
     * @param actionId the corporate action id
     */
    function _getSnapshotsSeriesID(
        uint256 actionId
    ) internal view returns (uint256[] memory) {
        bytes memory data = _getResult(actionId, SNAPSHOT_LIST_ID);

        uint256 bytesLength = data.length;
        uint256 totalSeries = bytesLength / 32;

        uint256[] memory series = new uint256[](totalSeries);

        for (uint256 i = 0; i < totalSeries; i++) {
            uint256 element;
            assembly {
                element := mload(add(data, add(0x20, mul(i, 0x20))))
            }
            series[i] = element;
        }

        return series;
    }

    //////////////////////////////////////////////////////////////////////
    // Serie Management //////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////

    function _setSerie(
        bytes memory metadata,
        bytes memory initializationArguments
    ) internal virtual returns (bool, uint256) {
        ISerie.SerieMetadata memory serieMetadata;
        serieMetadata.active = true;

        return
            _addPartition(
                abi.encode(serieMetadata, metadata),
                initializationArguments
            );
    }

    function _updateSerie(
        address serieAddress,
        bytes memory newMetadata
    ) internal virtual returns (bool) {
        (ISerie.SerieMetadata memory serieMetadata, ) = _getSerie(serieAddress);

        _updatePartition(serieAddress, abi.encode(serieMetadata, newMetadata));

        return true;
    }

    function _removeSerie(
        address serieAddress
    ) internal virtual returns (bool) {
        require(
            IERC20(serieAddress).totalSupply() == 0,
            'cannot remove Partition that still has tokens'
        );

        ISerie.SerieMetadata memory serieMetadata;
        serieMetadata.active = false;

        (, bytes memory metadata) = _getSerie(serieAddress);

        _updatePartition(serieAddress, abi.encode(serieMetadata, metadata));

        return true;
    }

    function _getSerie(
        address serieAddress
    )
        internal
        view
        virtual
        returns (ISerie.SerieMetadata memory, bytes memory)
    {
        bytes memory partitionMetadata = _getPartition(serieAddress);

        ISerie.SerieMetadata memory serieMetadata;

        if (partitionMetadata.length == 0) return (serieMetadata, '');

        return abi.decode(partitionMetadata, (ISerie.SerieMetadata, bytes));
    }

    function _getSeriesCount() internal view virtual returns (uint256) {
        return _getPartitionsCount();
    }

    /////////////////////////////////////////////////////////////////////
    // ERC 20 Compatibility ///////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////

    function transfer(
        address to,
        uint256 amount
    ) external override returns (bool) {
        revert('No yet implemented');
    }

    function allowance(
        address owner,
        address spender
    ) external view override returns (uint256) {
        revert('No yet implemented');
    }

    function approve(
        address spender,
        uint256 amount
    ) external override returns (bool) {
        revert('No yet implemented');
    }

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) external override returns (bool) {
        revert('No yet implemented');
    }
}
