// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {
    ICorporateActions
} from '../extensions/interfaces/ICorporateActions.sol';

interface ISecurity {
    function addCorporateActionSnapshot(
        uint256 actionId,
        uint256 snapshotId
    ) external;
}
