// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {
    IScheduledSnapshots
} from '../extensions/interfaces/IScheduledSnapshots.sol';

interface ISerie {
    struct SerieMetadata {
        bool active;
    }
}
