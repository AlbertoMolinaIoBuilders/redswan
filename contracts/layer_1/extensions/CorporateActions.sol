// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {ICorporateActions} from './interfaces/ICorporateActions.sol';

/*

Corporate actions are objects that are meant to be added to an array but never removed.
Corporate actions contain:
    - type : defines the type of action. Actions can be classified based on their type.
    - properties : defines tha action itself. Depending on the action type the properties element will probably be different.
    - results : any kind of extra information that must be added to the action, typically after the action is executed.

The "type" and "properties" of an action are supposed to be immutable.
The "result" field however can be updated at any time.

*/

contract CorporateActions is ICorporateActions {
    /////////////////////////////////////////
    // STATE VARIABLES /////////////////////////////////////////
    /////////////////////////////////////////
    /**
     * @dev _corporateActions is the list of all corporate actions
     */
    CorporateAction[] _corporateActions;

    /**
     * @dev _corporateActionsByType keeps a lists of the corporate actions ids (indexes in the _corporateActions array) per action type
     */
    mapping(bytes32 => uint256[]) _corporateActionsByType;

    /////////////////////////////////////////
    // MODIFIERS /////////////////////////////////////////
    /////////////////////////////////////////
    /**
     * @dev Checks if the action index exists
     *
     * @param index the action id we want to retrieve
     */
    modifier checkIndexForCorporateAction(uint256 index) {
        if (_getCorporateActionsCount() <= index) revert('Wrong index');
        _;
    }

    /**
     * @dev Checks if the action index exists for the provided action type
     *
     * @param actionType the corporate action type
     * @param index the action id (for that type) we want to retrieve
     */
    modifier checkIndexForCorporateActionByType(
        bytes32 actionType,
        uint256 index
    ) {
        if (_getCorporateActionsCountByType(actionType) <= index)
            revert('Wrong index for action');
        _;
    }

    /////////////////////////////////////////
    // ACTIONS MANAGEMENT /////////////////////////////////////////
    /////////////////////////////////////////
    /**
     * @dev Creates a corporate actions. Only action type and properties must be provided.
     *
     * @param actionType The corporate action type
     * @param properties The corporate action properties
     */
    function _addCorporateAction(
        bytes32 actionType,
        bytes memory properties
    ) internal virtual returns (uint256, uint256) {
        uint256 actionId = _corporateActions.length;
        uint256 actionIdForType = _corporateActionsByType[actionType].length;
        bytes[] memory emptyResults;

        _corporateActions.push(
            CorporateAction(actionType, properties, emptyResults)
        );

        _corporateActionsByType[actionType].push(actionId);

        return (actionId, actionIdForType);
    }

    /**
     * @dev Updates a corporate actions result. If the resultId points to a non-existing item of the action results array, it will create all the intermediate items.
     *
     * @param actionId The corporate action id
     * @param resultId The corporate action result id to update
     * @param newResult The result content
     */
    function _updateCorporateActionResult(
        uint256 actionId,
        uint256 resultId,
        bytes memory newResult
    ) internal virtual {
        bytes[] memory results = _corporateActions[actionId].results;

        if (results.length > resultId) {
            _corporateActions[actionId].results[resultId] = newResult;
            return;
        }

        for (uint256 i = results.length; i < resultId; i++) {
            _corporateActions[actionId].results.push('');
        }

        _corporateActions[actionId].results.push(newResult);
    }

    /**
     * @dev returns the corporate actions count by type.
     *
     */
    function _getCorporateActionsCountByType(
        bytes32 actionType
    ) internal view returns (uint256) {
        return _corporateActionsByType[actionType].length;
    }

    /**
     * @dev returns a corporate action Id by type.
     *
     * @param actionType The corporate action type
     * @param index The corporate action Id by type
     */
    function _getCorporateActionIdByType(
        bytes32 actionType,
        uint256 index
    ) internal view returns (uint256) {
        return _corporateActionsByType[actionType][index];
    }

    /**
     * @dev returns the corporate actions count.
     *
     */
    function _getCorporateActionsCount() internal view returns (uint256) {
        return _corporateActions.length;
    }

    /**
     * @dev returns a corporate action.
     *
     * @param actionId The corporate action Id
     */
    function _getCorporateAction(
        uint256 actionId
    ) internal view returns (CorporateAction storage) {
        return _corporateActions[actionId];
    }

    /**
     * @dev returns a corporate actions result count.
     *
     * @param actionId The corporate action Id
     */
    function _getCorporateActionResultsCount(
        uint256 actionId
    ) internal view returns (uint256) {
        return _corporateActions[actionId].results.length;
    }

    /**
     * @dev returns a corporate action result.
     *
     * @param actionId The corporate action Id
     */
    function _getCorporateActionResult(
        uint256 actionId,
        uint256 resultId
    ) internal view returns (bytes memory) {
        return _corporateActions[actionId].results[resultId];
    }
}
