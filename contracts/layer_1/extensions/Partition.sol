// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {IPartition} from './interfaces/IPartition.sol';
import {ScheduledSnapshots} from './ScheduledSnapshots.sol';

/*
    Partitions are defined for the ERC1400 standard.
    It is an ERC20 contract with 2 peculiarilities : 
        - it cannot be called directly, only thtough the Security contract the partition belongs to (Only view methods can be called directly)
        - it defines operators, which are basically like spenders with unlimited allowance

    Partitions must be initializable since all partitions within a security will refer to the same implementation (proxy beacon pattern)
    
    Partitions will keep in storage the address of their parent Security contract
*/

contract Partition is IPartition, ScheduledSnapshots {
    address internal _SECURITY_CONTRACT;
    uint8 private _decimals;
    mapping(address => mapping(address => bool)) internal _operators;

    modifier checkIsParentSecurity(address caller) {
        _checkIsParentSecurity(caller);
        _;
    }

    function _checkIsParentSecurity(address caller) private view {
        if (_SECURITY_CONTRACT != caller)
            revert('request not coming from parent security');
    }

    modifier checkIsNotOperator(address from, address operator) {
        _checkIsNotOperator(from, operator);
        _;
    }

    function _checkIsNotOperator(address from, address operator) private view {
        if (_isOperator(from, operator)) revert('it is an operator');
    }

    modifier checkIsOperator(address from, address operator) {
        _checkIsOperator(from, operator);
        _;
    }

    function _checkIsOperator(address from, address operator) private view {
        if (!_isOperator(from, operator)) revert('it is not an operator');
    }

    // Initializer
    function __Partition_init(
        address securityContract,
        uint8 initDecimals
    ) public onlyInitializing {
        __ScheduledSnapshots_init('', '');
        __Partition_init_unchained(securityContract, initDecimals);
    }

    function __Partition_init_unchained(
        address securityContract,
        uint8 initDecimals
    ) internal onlyInitializing {
        _SECURITY_CONTRACT = securityContract;
        _setDecimals(initDecimals);
    }

    // Token redeem
    function forceRedeem(
        address from,
        uint256 amount
    )
        external
        virtual
        override
        checkIsParentSecurity(msg.sender)
        returns (bool)
    {
        _burn(from, amount);
        return true;
    }

    function redeem(
        address originalSender,
        address from,
        uint256 amount
    )
        external
        virtual
        override
        checkIsParentSecurity(msg.sender)
        returns (bool)
    {
        if (originalSender == from) _burn(from, amount);
        else {
            _spendAllowance(from, originalSender, amount);
            _burn(from, amount);
        }

        return true;
    }

    // Snapshot
    function takeSnapshot()
        external
        virtual
        override
        checkIsParentSecurity(msg.sender)
        returns (uint256)
    {
        uint256 snapshotId = _triggerScheduledSnapshots(0);
        if (snapshotId != 0) return snapshotId;
        return _snapshot();
    }

    function addScheduledSnapshot(
        uint256 newScheduledTimestamp,
        bytes calldata newData
    )
        external
        virtual
        override
        checkTimestamp(newScheduledTimestamp)
        checkIsParentSecurity(msg.sender)
    {
        _addScheduledSnapshot(newScheduledTimestamp, newData);
    }

    // Token transfer
    function forceTransfer(
        address from,
        address to,
        uint256 amount
    )
        external
        virtual
        override
        checkIsParentSecurity(msg.sender)
        returns (bool)
    {
        _transfer(from, to, amount);
        return true;
    }

    function transfer(
        address originalSender,
        address from,
        address to,
        uint256 amount
    )
        external
        virtual
        override
        checkIsParentSecurity(msg.sender)
        returns (bool)
    {
        if (originalSender == from) _transfer(from, to, amount);
        else {
            _spendAllowance(from, originalSender, amount);
            _transfer(from, to, amount);
        }

        return true;
    }

    // Spenders
    function approve(
        address originalSender,
        address spender,
        uint256 amount
    )
        external
        virtual
        override
        checkIsParentSecurity(msg.sender)
        checkIsNotOperator(originalSender, spender)
        returns (bool)
    {
        _approve(originalSender, spender, amount);
        return true;
    }

    function increaseAllowance(
        address originalSender,
        address spender,
        uint256 addedValue
    )
        external
        virtual
        override
        checkIsParentSecurity(msg.sender)
        checkIsNotOperator(originalSender, spender)
        returns (bool)
    {
        _approve(
            originalSender,
            spender,
            allowance(originalSender, spender) + addedValue
        );
        return true;
    }

    function decreaseAllowance(
        address originalSender,
        address spender,
        uint256 subtractedValue
    )
        external
        virtual
        override
        checkIsParentSecurity(msg.sender)
        checkIsNotOperator(originalSender, spender)
        returns (bool)
    {
        uint256 currentAllowance = allowance(originalSender, spender);
        require(
            currentAllowance >= subtractedValue,
            'ERC20: decreased allowance below zero'
        );
        unchecked {
            _approve(
                originalSender,
                spender,
                currentAllowance - subtractedValue
            );
        }

        return true;
    }

    // Operator
    function setOperator(
        address originalSender,
        address operator
    )
        external
        virtual
        override
        checkIsParentSecurity(msg.sender)
        checkIsNotOperator(originalSender, operator)
        returns (bool)
    {
        _operators[originalSender][operator] = true;
        _approve(originalSender, operator, 0);
        return true;
    }

    function removeOperator(
        address originalSender,
        address operator
    )
        external
        virtual
        override
        checkIsParentSecurity(msg.sender)
        checkIsOperator(originalSender, operator)
        returns (bool)
    {
        _operators[originalSender][operator] = false;
        return true;
    }

    function isOperator(
        address from,
        address operator
    ) external view virtual override returns (bool) {
        return _isOperator(from, operator);
    }

    function _isOperator(
        address from,
        address operator
    ) private view returns (bool) {
        return _operators[from][operator];
    }

    // update the spend Allowance function to include operators (spenders with infinite allowance)
    function _spendAllowance(
        address owner,
        address spender,
        uint256 amount
    ) internal virtual override {
        if (_operators[owner][spender]) return;
        super._spendAllowance(owner, spender, amount);
    }

    // issue new tokens
    function issue(
        address account,
        uint256 amount
    ) external virtual override checkIsParentSecurity(msg.sender) {
        _mint(account, amount);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Deactivating all inherited public functions, partition contracts cannot be called directly frm users, only parent security can call it.
    ////////////////////////////////////////////////////////////////////////////
    function transfer(
        address to,
        uint256 amount
    ) public virtual override returns (bool) {
        return false;
    }

    function approve(
        address spender,
        uint256 amount
    ) public virtual override returns (bool) {
        return false;
    }

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) public virtual override returns (bool) {
        return false;
    }

    function increaseAllowance(
        address spender,
        uint256 addedValue
    ) public virtual override returns (bool) {
        return false;
    }

    function decreaseAllowance(
        address spender,
        uint256 subtractedValue
    ) public virtual override returns (bool) {
        return false;
    }

    // Decimals
    function decimals() public view virtual override returns (uint8) {
        return _decimals;
    }

    /* 
        Updates the number of decimals and saves it in the current snapshot
    */

    function _setDecimals(uint8 newDecimals) internal virtual {
        _decimals = newDecimals;
    }
}
