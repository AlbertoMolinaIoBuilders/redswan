// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

interface IPartition {
    /**
     * @dev a partition is defined by its "metadata".
     * The id is the one-based index of the partition within a security, which means that if the Security is listing the partitions within an array then :
     * array index = id - 1
     * The id of a partition must never be changed after its creation
     *
     *
     * @params id the one-based id of the partition
     * @params metadata partition metadata
     */
    struct PartitionMetadata {
        uint256 id;
        bytes metadata;
    }

    function forceTransfer(
        address from,
        address to,
        uint256 amount
    ) external returns (bool);

    function transfer(
        address originalSender,
        address from,
        address to,
        uint256 amount
    ) external returns (bool);

    function forceRedeem(address from, uint256 amount) external returns (bool);

    function redeem(
        address originalSender,
        address from,
        uint256 amount
    ) external returns (bool);

    function approve(
        address originalSender,
        address spender,
        uint256 amount
    ) external returns (bool);

    function increaseAllowance(
        address originalSender,
        address spender,
        uint256 addedValue
    ) external returns (bool);

    function decreaseAllowance(
        address originalSender,
        address spender,
        uint256 subtractedValue
    ) external returns (bool);

    function issue(address account, uint256 amount) external;

    function setOperator(
        address originalSender,
        address operator
    ) external returns (bool);

    function removeOperator(
        address originalSender,
        address operator
    ) external returns (bool);

    function isOperator(
        address from,
        address operator
    ) external view returns (bool);
}
