// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

import {Serie} from './layer_1/Serie.sol';
import {IBondSerie} from './interfaces/IBondSerie.sol';

/*
    Series extend Partitions (as defined at the ERC1400 level) with all the modules that apply :
    - Snapshots : snapshots must be schedulable at the partition level => ScheduledSnapshots
    - Corporate Actions : corporate actions info must be updated when a scheduled snapshot is executed
    - Caps : we must be able to set max supplies per partition

    Series will keep in storage the serie Id (aka partition Id) they have been assigned by their parent Security contract, 
    this information must be provided to the parent Security contract when communicating with it, so that the parent knows
*/

contract BondSerie is IBondSerie, Serie {
    /**
     * @dev Constructor required to avoid Initializer attack on logic contract
     */
    constructor() {
        //_disableInitializers();
    }

    // Initialization /////////////////

    function __BondSerie_init(
        address securityContract,
        uint8 initDecimals
    ) public initializer {
        __Serie_init(securityContract, initDecimals);
    }
}
