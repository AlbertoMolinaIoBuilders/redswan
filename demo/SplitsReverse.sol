// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.16;

contract SplitReverse {
    // ERC-20
    mapping(address => uint256) _balances;
    uint256 public _totalSupply;
    uint256 public _decimals;

    // Split & Reverse Split
    mapping(address => uint256) _lastFactor;
    uint256 public _aggregatedFactor;

    constructor(uint256 decimals) {
        _decimals = decimals;
        _aggregatedFactor = 1;
    }

    function mint(uint256 amount, address account) external {
        updateBalanceOf(account);

        _balances[account] += amount;
        _totalSupply += amount;
    }

    function burn(uint256 amount, address account) external {
        updateBalanceOf(account);

        require(_balances[account] >= amount, 'Not enough balance');

        _balances[account] -= amount;
        _totalSupply -= amount;
    }

    function transfer(uint256 amount, address account) external {
        require(amount > 0, 'Must transfer something');

        updateBalanceOf(msg.sender);

        require(_balances[msg.sender] >= amount, 'Not enough balance');

        updateBalanceOf(account);

        _balances[msg.sender] -= amount;
        _balances[account] += amount;
    }

    function getBalance(address account) external view returns (uint256) {
        uint256 balance = _balances[account];
        if (balance == 0) return balance;
        if (_lastFactor[account] != _aggregatedFactor) {
            uint256 diff_factor = _aggregatedFactor / _lastFactor[account];
            balance *= diff_factor;
        }

        return balance;
    }

    function getLastFactor(address account) external view returns (uint256) {
        return _lastFactor[account];
    }

    function updateBalanceOf(address account) internal {
        if (_lastFactor[account] != _aggregatedFactor) {
            if (_lastFactor[account] != 0) {
                uint256 diff_factor = _aggregatedFactor / _lastFactor[account];
                _balances[account] *= diff_factor;
            }
            _lastFactor[account] = _aggregatedFactor;
        }
    }

    /* 
        Must be used when updating the supply with an integer (no decimals!!!!)
        - Split 3 => factor = 3; decimals = 0;
        - Split 30 => factor = 3; decimals = 1;
        - Split 1000 => factor = 1; decimals = 3;
        - Split 13.55 => NOT POSSIBLE, USE "SplittingWithDecimals for this
    */
    function SplitingNoDecimals(uint256 factor, uint256 decimals) external {
        if (decimals > _decimals) {
            factor *= 10 ** (decimals - _decimals);
            decimals = _decimals;
        }
        _decimals -= decimals;
        _aggregatedFactor *= factor;
        _totalSupply *= factor;
    }

    /* 
       Must be used when updating the supply with a decimal
        - Split 3.5 => factor = 35; decimals = 1;
        - ReverseSplit 0.3 => factor = 3; decimals = 1;
        - ReverseSplit 0.005 => factor = 5; decimals = 3;

        Caveats : reverse spliting by infinite fractions (like 1/3) will require rounding it up to X decimals (i.e factor = 3333; decimals = 4)
    */
    function SplittingWithDecimals(uint256 factor, uint256 decimals) external {
        _decimals += decimals;
        _aggregatedFactor *= factor;
        _totalSupply *= factor;
    }
}
