# Security









## Methods

### addCorporateActionSnapshot

```solidity
function addCorporateActionSnapshot(uint256 actionId, uint256 snapshotId) external nonpayable
```



*Adds a corporate actions snapshot id to the results. This method is meant to be called exclusively from one of the Security&#39;s active Series contracts.   Snapshots will be stored in the following way   slot 1 : Serie Id 1 (index + 1)   slot 2 : SnapShot 1   slot 3 : Serie Id 2 (index + 1)   slot 4 : SnapShot 2   ...   slot n-1 : Serie Id n/2 (index + 1)   slot n : SnapShot n/2*

#### Parameters

| Name | Type | Description |
|---|---|---|
| actionId | uint256 | The corporate action Id that must be updated |
| snapshotId | uint256 | the snapshot Id to add to the corporate action |

### allowance

```solidity
function allowance(address owner, address spender) external view returns (uint256)
```



*Returns the remaining number of tokens that `spender` will be allowed to spend on behalf of `owner` through {transferFrom}. This is zero by default. This value changes when {approve} or {transferFrom} are called.*

#### Parameters

| Name | Type | Description |
|---|---|---|
| owner | address | undefined |
| spender | address | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### approve

```solidity
function approve(address spender, uint256 amount) external nonpayable returns (bool)
```



*Sets `amount` as the allowance of `spender` over the caller&#39;s tokens. Returns a boolean value indicating whether the operation succeeded. IMPORTANT: Beware that changing an allowance with this method brings the risk that someone may use both the old and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this race condition is to first reduce the spender&#39;s allowance to 0 and set the desired value afterwards: https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729 Emits an {Approval} event.*

#### Parameters

| Name | Type | Description |
|---|---|---|
| spender | address | undefined |
| amount | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |

### balanceOf

```solidity
function balanceOf(address account) external view returns (uint256)
```



*Returns the amount of tokens owned by `account`.*

#### Parameters

| Name | Type | Description |
|---|---|---|
| account | address | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### balanceOfByPartition

```solidity
function balanceOfByPartition(uint256 partitionId, address tokenHolder) external view returns (uint256)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| partitionId | uint256 | undefined |
| tokenHolder | address | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### decimals

```solidity
function decimals() external view returns (uint8)
```






#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint8 | undefined |

### decimalsByPartition

```solidity
function decimalsByPartition(uint256 partitionId) external view returns (uint8)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| partitionId | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint8 | undefined |

### issueByPartition

```solidity
function issueByPartition(uint256 partitionId, address tokenHolder, uint256 value, bytes data) external nonpayable
```

AuthorizeOperator / RevokeOperatorfrom / isOperator from ALL partitions of * the &quot;msg.sender&quot;.



#### Parameters

| Name | Type | Description |
|---|---|---|
| partitionId | uint256 | undefined |
| tokenHolder | address | undefined |
| value | uint256 | undefined |
| data | bytes | undefined |

### totalSupply

```solidity
function totalSupply() external view returns (uint256)
```



*Returns the amount of tokens in existence.*


#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### totalSupplyByPartition

```solidity
function totalSupplyByPartition(uint256 partitionId) external view returns (uint256)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| partitionId | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### transfer

```solidity
function transfer(address to, uint256 amount) external nonpayable returns (bool)
```



*Moves `amount` tokens from the caller&#39;s account to `to`. Returns a boolean value indicating whether the operation succeeded. Emits a {Transfer} event.*

#### Parameters

| Name | Type | Description |
|---|---|---|
| to | address | undefined |
| amount | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |

### transferFrom

```solidity
function transferFrom(address from, address to, uint256 amount) external nonpayable returns (bool)
```



*Moves `amount` tokens from `from` to `to` using the allowance mechanism. `amount` is then deducted from the caller&#39;s allowance. Returns a boolean value indicating whether the operation succeeded. Emits a {Transfer} event.*

#### Parameters

| Name | Type | Description |
|---|---|---|
| from | address | undefined |
| to | address | undefined |
| amount | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |



## Events

### Approval

```solidity
event Approval(address indexed owner, address indexed spender, uint256 value)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| owner `indexed` | address | undefined |
| spender `indexed` | address | undefined |
| value  | uint256 | undefined |

### Transfer

```solidity
event Transfer(address indexed from, address indexed to, uint256 value)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| from `indexed` | address | undefined |
| to `indexed` | address | undefined |
| value  | uint256 | undefined |



