# IScheduledSnapshots









## Methods

### addScheduledSnapshot

```solidity
function addScheduledSnapshot(uint256 newScheduledTimestamp, bytes newData) external nonpayable
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| newScheduledTimestamp | uint256 | undefined |
| newData | bytes | undefined |

### takeSnapshot

```solidity
function takeSnapshot() external nonpayable returns (uint256)
```






#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### triggerPendingScheduledSnapshots

```solidity
function triggerPendingScheduledSnapshots() external nonpayable returns (uint256)
```






#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### triggerScheduledSnapshots

```solidity
function triggerScheduledSnapshots(uint256 max) external nonpayable returns (uint256)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| max | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |




