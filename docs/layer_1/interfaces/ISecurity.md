# ISecurity









## Methods

### addCorporateActionSnapshot

```solidity
function addCorporateActionSnapshot(uint256 actionId, uint256 snapshotId) external nonpayable
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| actionId | uint256 | undefined |
| snapshotId | uint256 | undefined |




