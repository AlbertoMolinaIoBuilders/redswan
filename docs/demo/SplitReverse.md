# SplitReverse









## Methods

### SplitingNoDecimals

```solidity
function SplitingNoDecimals(uint256 factor, uint256 decimals) external nonpayable
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| factor | uint256 | undefined |
| decimals | uint256 | undefined |

### SplittingWithDecimals

```solidity
function SplittingWithDecimals(uint256 factor, uint256 decimals) external nonpayable
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| factor | uint256 | undefined |
| decimals | uint256 | undefined |

### _aggregatedFactor

```solidity
function _aggregatedFactor() external view returns (uint256)
```






#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### _decimals

```solidity
function _decimals() external view returns (uint256)
```






#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### _totalSupply

```solidity
function _totalSupply() external view returns (uint256)
```






#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### burn

```solidity
function burn(uint256 amount, address account) external nonpayable
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| amount | uint256 | undefined |
| account | address | undefined |

### getBalance

```solidity
function getBalance(address account) external view returns (uint256)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| account | address | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### getLastFactor

```solidity
function getLastFactor(address account) external view returns (uint256)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| account | address | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### mint

```solidity
function mint(uint256 amount, address account) external nonpayable
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| amount | uint256 | undefined |
| account | address | undefined |

### transfer

```solidity
function transfer(uint256 amount, address account) external nonpayable
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| amount | uint256 | undefined |
| account | address | undefined |




