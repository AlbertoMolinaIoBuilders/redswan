# IEquitySerie









## Methods

### SplittingNoDecimals

```solidity
function SplittingNoDecimals(uint256 factor, uint8 subtractDecimals) external nonpayable
```

methods



#### Parameters

| Name | Type | Description |
|---|---|---|
| factor | uint256 | undefined |
| subtractDecimals | uint8 | undefined |

### SplittingWithDecimals

```solidity
function SplittingWithDecimals(uint256 factor, uint8 addDecimals) external nonpayable
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| factor | uint256 | undefined |
| addDecimals | uint8 | undefined |




