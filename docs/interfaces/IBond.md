# IBond









## Methods

### createBondSerie

```solidity
function createBondSerie(IBondSerie.BondMetadata bondMetadata, bytes metadata, uint8 initialDecimals, bytes initializationArguments) external nonpayable returns (bool, uint256)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| bondMetadata | IBondSerie.BondMetadata | undefined |
| metadata | bytes | undefined |
| initialDecimals | uint8 | undefined |
| initializationArguments | bytes | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |
| _1 | uint256 | undefined |

### getBondSerie

```solidity
function getBondSerie(uint256 serieId) external view returns (address, struct ISerie.SerieMetadata, struct IBondSerie.BondMetadata, bytes)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| serieId | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | address | undefined |
| _1 | ISerie.SerieMetadata | undefined |
| _2 | IBondSerie.BondMetadata | undefined |
| _3 | bytes | undefined |

### getBondSerieCount

```solidity
function getBondSerieCount() external view returns (uint256)
```






#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### getCoupon

```solidity
function getCoupon(uint256 couponId) external view returns (struct IBond.RegisteredCoupon, bytes)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| couponId | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | IBond.RegisteredCoupon | undefined |
| _1 | bytes | undefined |

### getCouponsCount

```solidity
function getCouponsCount() external view returns (uint256)
```






#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### removeBondSerie

```solidity
function removeBondSerie(uint256 serieId) external nonpayable returns (bool)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| serieId | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |

### setCoupon

```solidity
function setCoupon(IBond.Coupon newCoupon, bytes metadata) external nonpayable returns (bool, uint256)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| newCoupon | IBond.Coupon | undefined |
| metadata | bytes | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |
| _1 | uint256 | undefined |

### updateBondSerie

```solidity
function updateBondSerie(uint256 serieId, IBondSerie.BondMetadata bondMetadata, bytes newMetadata) external nonpayable returns (bool)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| serieId | uint256 | undefined |
| bondMetadata | IBondSerie.BondMetadata | undefined |
| newMetadata | bytes | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |




