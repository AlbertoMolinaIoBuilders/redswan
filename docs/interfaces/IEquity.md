# IEquity









## Methods

### createEquitySerie

```solidity
function createEquitySerie(IEquitySerie.EquityMetadata equityMetadata, bytes metadata, uint8 initialDecimals, bytes initializationArguments) external nonpayable returns (bool, uint256)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| equityMetadata | IEquitySerie.EquityMetadata | undefined |
| metadata | bytes | undefined |
| initialDecimals | uint8 | undefined |
| initializationArguments | bytes | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |
| _1 | uint256 | undefined |

### getDividends

```solidity
function getDividends(uint256 dividendId) external view returns (struct IEquity.RegisteredDividend, bytes)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| dividendId | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | IEquity.RegisteredDividend | undefined |
| _1 | bytes | undefined |

### getDividendsCount

```solidity
function getDividendsCount() external view returns (uint256)
```






#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### getEquitySerie

```solidity
function getEquitySerie(uint256 serieId) external view returns (address, struct ISerie.SerieMetadata, struct IEquitySerie.EquityMetadata, bytes)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| serieId | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | address | undefined |
| _1 | ISerie.SerieMetadata | undefined |
| _2 | IEquitySerie.EquityMetadata | undefined |
| _3 | bytes | undefined |

### getEquitySerieCount

```solidity
function getEquitySerieCount() external view returns (uint256)
```






#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### getSplit

```solidity
function getSplit(uint256 splitId) external view returns (struct IEquity.RegisteredSplit, bytes)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| splitId | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | IEquity.RegisteredSplit | undefined |
| _1 | bytes | undefined |

### getSplitsCount

```solidity
function getSplitsCount() external view returns (uint256)
```






#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | uint256 | undefined |

### removeEquitySerie

```solidity
function removeEquitySerie(uint256 serieId) external nonpayable returns (bool)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| serieId | uint256 | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |

### setDividends

```solidity
function setDividends(IEquity.Dividend newDividend, bytes metadata) external nonpayable returns (bool, uint256)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| newDividend | IEquity.Dividend | undefined |
| metadata | bytes | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |
| _1 | uint256 | undefined |

### setSplit

```solidity
function setSplit(IEquity.Split newSplit, bytes metadata) external nonpayable returns (bool, uint256)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| newSplit | IEquity.Split | undefined |
| metadata | bytes | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |
| _1 | uint256 | undefined |

### updateEquitySerie

```solidity
function updateEquitySerie(uint256 serieId, IEquitySerie.EquityMetadata equityMetadata, bytes newMetadata) external nonpayable returns (bool)
```





#### Parameters

| Name | Type | Description |
|---|---|---|
| serieId | uint256 | undefined |
| equityMetadata | IEquitySerie.EquityMetadata | undefined |
| newMetadata | bytes | undefined |

#### Returns

| Name | Type | Description |
|---|---|---|
| _0 | bool | undefined |




